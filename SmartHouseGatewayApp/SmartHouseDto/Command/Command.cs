﻿using System;
namespace SmartHouseDto.Command
{
    public class Command
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
